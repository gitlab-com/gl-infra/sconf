# sconf

`sconf` stores configured directories in an encrypted archive at rest. When unlocked, these are unpacked into a ramdisk and symlinked into place.

Currently stored directories are: `.config` `.gsutil` `.kube`.

Only Linux systems with `/dev/shm` are supported at the moment. OpenSSL is required.

### Usage:

#### Installation:

```
git clone https://gitlab.com/gitlab-com/gl-infra/sconf.git "${HOME}/sconf"
echo 'export PATH="${PATH}:${HOME}/sconf"' >> ~/.bashrc
```

#### Initial config import:

```
sconf import
<type password twice>
```

#### Open sconf vault:

```
sconf load
<type password>
```

#### Save changes:

```
sconf save
<type password twice>
```

#### Abandon ship / Lock it up (changes are not saved!)

```
sconf exit
```

### License

MIT License

Copyright (c) 2020 GitLab.com / GitLab Infrastructure Team
